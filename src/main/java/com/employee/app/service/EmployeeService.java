package com.employee.app.service;

import com.employee.app.model.Employee;

import java.util.List;
import java.util.Optional;


public interface EmployeeService {
    public List<Employee> retrieveEmployees();

    public Optional<Employee> getEmployee(Long employeeId);

    public Employee saveEmployee(Employee employee);

    public void deleteEmployee(Long employeeId);

    public Employee updateEmployee(Employee employee);
}